'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var React = _interopRequireWildcard(_react);

var _rcDialog = require('rc-dialog');

var _rcDialog2 = _interopRequireDefault(_rcDialog);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _addEventListener = require('rc-util/lib/Dom/addEventListener');

var _addEventListener2 = _interopRequireDefault(_addEventListener);

var _button = require('../button');

var _button2 = _interopRequireDefault(_button);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj['default'] = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var mousePosition = void 0;
var mousePositionEventBinded = void 0;
var Footer = function Footer(props) {
    var okText = props.okText,
        okType = props.okType,
        cancelText = props.cancelText,
        confirmLoading = props.confirmLoading,
        handleCancel = props.handleCancel,
        handleOk = props.handleOk;

    return React.createElement(
        'div',
        null,
        React.createElement(
            _button2['default'],
            { onClick: handleCancel },
            cancelText
        ),
        React.createElement(
            _button2['default'],
            { type: okType, loading: confirmLoading, onClick: handleOk },
            okText
        )
    );
};

var Modal = function (_React$Component) {
    (0, _inherits3['default'])(Modal, _React$Component);

    function Modal() {
        (0, _classCallCheck3['default'])(this, Modal);

        var _this = (0, _possibleConstructorReturn3['default'])(this, (Modal.__proto__ || Object.getPrototypeOf(Modal)).apply(this, arguments));

        _this.handleCancel = function (e) {
            var onCancel = _this.props.onCancel;
            if (onCancel) {
                onCancel(e);
            }
        };
        _this.handleOk = function (e) {
            var onOk = _this.props.onOk;
            if (onOk) {
                onOk(e);
            }
        };
        return _this;
    }

    (0, _createClass3['default'])(Modal, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            if (mousePositionEventBinded) {
                return;
            }
            // 只有点击事件支持从鼠标位置动画展开
            (0, _addEventListener2['default'])(document.documentElement, 'click', function (e) {
                mousePosition = {
                    x: e.pageX,
                    y: e.pageY
                };
                // 100ms 内发生过点击事件，则从点击位置动画展示
                // 否则直接 zoom 展示
                // 这样可以兼容非点击方式展开
                setTimeout(function () {
                    return mousePosition = null;
                }, 100);
            });
            mousePositionEventBinded = true;
        }
        // renderFooter = (locale: ModalLocale) => {
        //   const { okText, okType, cancelText, confirmLoading } = this.props;
        //   return (
        //     <div>
        //       <Button
        //         onClick={this.handleCancel}
        //       >
        //         {cancelText || locale.cancelText}
        //       </Button>
        //       <Button
        //         type={okType}
        //         loading={confirmLoading}
        //         onClick={this.handleOk}
        //       >
        //         {okText || locale.okText}
        //       </Button>
        //     </div>
        //   );
        // }

    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                footer = _props.footer,
                visible = _props.visible;

            var defaultFooter = React.createElement(Footer, { okText: '\u786E\u5B9A', okType: 'primary', cancelText: '\u53D6\u6D88', confirmLoading: '' });
            return React.createElement(_rcDialog2['default'], (0, _extends3['default'])({}, this.props, { footer: footer === undefined ? defaultFooter : footer, visible: visible, mousePosition: mousePosition, onClose: this.handleCancel }));
        }
    }]);
    return Modal;
}(React.Component);

exports['default'] = Modal;

Modal.defaultProps = {
    prefixCls: 'ant-modal',
    width: 520,
    transitionName: 'zoom',
    maskTransitionName: 'fade',
    confirmLoading: false,
    visible: false,
    okType: 'primary'
};
Modal.propTypes = {
    prefixCls: _propTypes2['default'].string,
    onOk: _propTypes2['default'].func,
    onCancel: _propTypes2['default'].func,
    okText: _propTypes2['default'].node,
    cancelText: _propTypes2['default'].node,
    width: _propTypes2['default'].oneOfType([_propTypes2['default'].number, _propTypes2['default'].string]),
    confirmLoading: _propTypes2['default'].bool,
    visible: _propTypes2['default'].bool,
    align: _propTypes2['default'].object,
    footer: _propTypes2['default'].node,
    title: _propTypes2['default'].node,
    closable: _propTypes2['default'].bool
};
module.exports = exports['default'];