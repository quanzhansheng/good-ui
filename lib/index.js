'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _alert = require('./alert');

Object.defineProperty(exports, 'Alert', {
    enumerable: true,
    get: function get() {
        return _interopRequireDefault(_alert)['default'];
    }
});

var _checkbox = require('./checkbox');

Object.defineProperty(exports, 'Checkbox', {
    enumerable: true,
    get: function get() {
        return _interopRequireDefault(_checkbox)['default'];
    }
});

var _input = require('./input');

Object.defineProperty(exports, 'Input', {
    enumerable: true,
    get: function get() {
        return _interopRequireDefault(_input)['default'];
    }
});

var _message = require('./message');

Object.defineProperty(exports, 'message', {
    enumerable: true,
    get: function get() {
        return _interopRequireDefault(_message)['default'];
    }
});

var _modal = require('./modal');

Object.defineProperty(exports, 'Modal', {
    enumerable: true,
    get: function get() {
        return _interopRequireDefault(_modal)['default'];
    }
});

var _radio = require('./radio');

Object.defineProperty(exports, 'Radio', {
    enumerable: true,
    get: function get() {
        return _interopRequireDefault(_radio)['default'];
    }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

/* @remove-on-es-build-begin */
// this file is not used if use https://github.com/ant-design/babel-plugin-import
var ENV = process.env.NODE_ENV;
if (ENV !== 'production' && typeof console !== 'undefined' && console.warn && typeof window !== 'undefined') {
    console.warn('You are using a whole package of antd, ' + 'please use https://www.npmjs.com/package/babel-plugin-import to reduce app bundle size.');
}
/* @remove-on-es-build-end */