import _extends from 'babel-runtime/helpers/extends';
import _classCallCheck from 'babel-runtime/helpers/classCallCheck';
import _createClass from 'babel-runtime/helpers/createClass';
import _possibleConstructorReturn from 'babel-runtime/helpers/possibleConstructorReturn';
import _inherits from 'babel-runtime/helpers/inherits';
import * as React from 'react';
import Dialog from 'rc-dialog';
import PropTypes from 'prop-types';
import addEventListener from 'rc-util/es/Dom/addEventListener';
import Button from '../button';
var mousePosition = void 0;
var mousePositionEventBinded = void 0;
var Footer = function Footer(props) {
    var okText = props.okText,
        okType = props.okType,
        cancelText = props.cancelText,
        confirmLoading = props.confirmLoading,
        handleCancel = props.handleCancel,
        handleOk = props.handleOk;

    return React.createElement(
        'div',
        null,
        React.createElement(
            Button,
            { onClick: handleCancel },
            cancelText
        ),
        React.createElement(
            Button,
            { type: okType, loading: confirmLoading, onClick: handleOk },
            okText
        )
    );
};

var Modal = function (_React$Component) {
    _inherits(Modal, _React$Component);

    function Modal() {
        _classCallCheck(this, Modal);

        var _this = _possibleConstructorReturn(this, (Modal.__proto__ || Object.getPrototypeOf(Modal)).apply(this, arguments));

        _this.handleCancel = function (e) {
            var onCancel = _this.props.onCancel;
            if (onCancel) {
                onCancel(e);
            }
        };
        _this.handleOk = function (e) {
            var onOk = _this.props.onOk;
            if (onOk) {
                onOk(e);
            }
        };
        return _this;
    }

    _createClass(Modal, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            if (mousePositionEventBinded) {
                return;
            }
            // 只有点击事件支持从鼠标位置动画展开
            addEventListener(document.documentElement, 'click', function (e) {
                mousePosition = {
                    x: e.pageX,
                    y: e.pageY
                };
                // 100ms 内发生过点击事件，则从点击位置动画展示
                // 否则直接 zoom 展示
                // 这样可以兼容非点击方式展开
                setTimeout(function () {
                    return mousePosition = null;
                }, 100);
            });
            mousePositionEventBinded = true;
        }
        // renderFooter = (locale: ModalLocale) => {
        //   const { okText, okType, cancelText, confirmLoading } = this.props;
        //   return (
        //     <div>
        //       <Button
        //         onClick={this.handleCancel}
        //       >
        //         {cancelText || locale.cancelText}
        //       </Button>
        //       <Button
        //         type={okType}
        //         loading={confirmLoading}
        //         onClick={this.handleOk}
        //       >
        //         {okText || locale.okText}
        //       </Button>
        //     </div>
        //   );
        // }

    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                footer = _props.footer,
                visible = _props.visible;

            var defaultFooter = React.createElement(Footer, { okText: '\u786E\u5B9A', okType: 'primary', cancelText: '\u53D6\u6D88', confirmLoading: '' });
            return React.createElement(Dialog, _extends({}, this.props, { footer: footer === undefined ? defaultFooter : footer, visible: visible, mousePosition: mousePosition, onClose: this.handleCancel }));
        }
    }]);

    return Modal;
}(React.Component);

export default Modal;

Modal.defaultProps = {
    prefixCls: 'ant-modal',
    width: 520,
    transitionName: 'zoom',
    maskTransitionName: 'fade',
    confirmLoading: false,
    visible: false,
    okType: 'primary'
};
Modal.propTypes = {
    prefixCls: PropTypes.string,
    onOk: PropTypes.func,
    onCancel: PropTypes.func,
    okText: PropTypes.node,
    cancelText: PropTypes.node,
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    confirmLoading: PropTypes.bool,
    visible: PropTypes.bool,
    align: PropTypes.object,
    footer: PropTypes.node,
    title: PropTypes.node,
    closable: PropTypes.bool
};